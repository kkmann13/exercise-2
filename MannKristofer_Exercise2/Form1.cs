﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace MannKristofer_Exercise2
{
    public partial class Form1 : Form
    {
        //public event handler for displaying data from listview
        public event EventHandler DisplayData;

        //get set contact data from textboxes and checkboxes
        public Contacts Data
        {
            get
            {
                Contacts c = new Contacts();
                c.First = firstNameTextBox.Text;
                c.Last = lastNameTextBox.Text;
                c.Num = phoneNumTextBox.Text;
                c.Email = emailTextBox.Text;
                if (maleCheckbox.Checked == true)
                {
                    c.Male = maleCheckbox.Checked;
                    femaleCheckBox.Checked = false;
                    c.Index = 0;
                }else if (femaleCheckBox.Checked == true)
                {
                    c.Female = femaleCheckBox.Checked;
                    maleCheckbox.Checked = false;
                    c.Index = 1;
                }

                return c;
            }

            set
            {
                firstNameTextBox.Text = value.First;
                lastNameTextBox.Text = value.Last;
                phoneNumTextBox.Text = value.Num;
                emailTextBox.Text = value.Email;
                maleCheckbox.Checked = value.Male;
                femaleCheckBox.Checked = value.Female;
            }
        }


        public Form1()
        {
            InitializeComponent();
        }

        //identify selected item from listview
        public Contacts SelectedContact
        {
            get
            {
                if (contactListView.SelectedItems.Count > 0)
                {
                    return contactListView.SelectedItems[0].Tag as Contacts;
                }
                else { return new Contacts(); }
            }
        }

        //method for submitting contact info into list and clear text/checks
        private void submitButton_Click(object sender, EventArgs e)
        {
            ListViewItem lvi = new ListViewItem();
            lvi.Text = Data.First + " " + Data.Last;
            lvi.ImageIndex = Data.Index;
            lvi.Tag = Data;

            contactListView.Items.Add(lvi);
            DisplayData += HandlerDisplayData;
            applyButton.Visible = false;
            deleteButton.Visible = false;
            Data = new Contacts();
        }

        //changes female checkbox to false
        private void maleCheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (maleCheckbox.Checked == true)
            {
                femaleCheckBox.Checked = false;
            }
        }

        //changes male checkbox to false
        private void femaleCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (femaleCheckBox.Checked == true)
            {
                maleCheckbox.Checked = false;
            }
        }

        //selects an item from the listview and displays its info
        private void listView1_MouseClick(object sender, MouseEventArgs e)
        {
            if (DisplayData != null)
            {
                DisplayData(this, new EventArgs());
            }
        }

        //handler for making apply and delete button true plus display data
        public  void HandlerDisplayData(object sender, EventArgs e)
        {
            applyButton.Visible = true;
            deleteButton.Visible = true;
            Data = SelectedContact;
        }

        //exit application for new menu strip
        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //my original menu strip disapeared and I cannot find it.
        //it will not let me delete this exit tool strip. I do not know what happened.
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //makes the icons 16x16
        private void smallToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            largeToolStripMenuItem1.Text = "Large";
            smallToolStripMenuItem1.Text = "Small \u2713";
            contactListView.View = View.SmallIcon;
        }

        //makes the icons 32x32
        private void largeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            smallToolStripMenuItem1.Text = "Small";
            largeToolStripMenuItem1.Text = "Large \u2713";
            contactListView.View = View.LargeIcon;
        }

        //updates selected contact's info
        private void applyButton_Click(object sender, EventArgs e)
        {
            ListViewItem lvi = new ListViewItem();
            lvi.Text = Data.First + " " + Data.Last;
            lvi.ImageIndex = Data.Index;
            lvi.Tag = Data;
            
            contactListView.Items.Add(lvi);
            
            if (contactListView.SelectedItems.Count > 0)
            {
                contactListView.SelectedItems[0].Remove();
                Data = new Contacts();
            }
            else { Data = new Contacts(); }
        }

        //deletes selected contact from list
        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (contactListView.SelectedItems.Count > 0)
            {
                contactListView.SelectedItems[0].Remove();
                Data = new Contacts();
            }
            else { Data = new Contacts(); }           
        }

        //saves the current contacts info to local machine
        private void saveToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            List<Contacts> DataList = new List<Contacts>();
            DataList.Add(Data);

            SaveFileDialog sfd = new SaveFileDialog();

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter sw = new StreamWriter(sfd.FileName + @".txt"))
                {
                    foreach(Contacts c in DataList)
                    {
                        sw.WriteLine(c.SaveContact());
                    }
                }
            }
        }
    }
}
