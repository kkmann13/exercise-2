﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MannKristofer_Exercise2
{
   public class Contacts
    {
        //default constructor
        public Contacts() { }

        //declaring variables
        string first;
        string last;
        string num;
        string email;
        bool male;
        bool female;
        int index;

        //get set first name
        public string First
        {
            get { return first; }
            set { first = value; }
        }

        //get set last name
        public string Last
        {
            get { return last; }
            set { last = value; }
        }

        //get set phone number
        public string Num
        {
            get { return num; }
            set { num = value; }
        }

        //get set email
        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        //get set male
        public bool Male
        {
            get { return male; }
            set { male = value; }
        }

        //get set female
        public bool Female
        {
            get { return female; }
            set { female = value; }
        }

        //get set image index
        public int Index
        {
            get { return index; }
            set { index = value; }
        }

        //override tostring method
        public override string ToString()
        {
            return First + Last + Num + Email + Male + Female;
        }

        //method to save contact info
        public string SaveContact()
        {
            string cData;
            cData = First + "," + Last + "," + Num + "," + Email+ "," +Male+ "," +Female+ "," + Index;

            return cData;
        }
    }
}
